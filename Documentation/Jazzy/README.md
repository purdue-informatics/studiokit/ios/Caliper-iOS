Caliper iOS is a [Caliper](http://www.imsglobal.org/activity/caliperram) 1.0 Sensor Library. Written in Swift, it supports both Swift & Objective-C iOS apps.

## Installation

### Cocoapods
To add Caliper iOS to your project using CocoaPods, add it to your `Podfile`:  

	source 'https://github.com/CocoaPods/Specs.git'
	platform :ios, '9.0'
	use_frameworks!

	target '<Your Target Name>' do
	    pod 'Caliper-iOS', '~> 1.0.0'
	end

### Carthage
For Carthage, add it to your `Cartfile`:  

	github "https://github.com/gavin-brown/Caliper-iOS"

## Usage
### Swift
```swift 
let options = Options()
options.host = "Your Event Store endpoint"
options.apiKey = "Your Event Store API key or authorization token"

let client = Client(options: options)
let sensor = Sensor(identifier: "Your sensor IRI", client: self.client)

let person = Person(identifier: "Your person IRI")
person.name = "Your person's name"

let app = SoftwareApplication(identifier: "Your app IRI")
app.name = "Your app's name"

let session = Session(identifier: "Your session IRI", actor: person)
session.name = "Your session name"
session.startedAtTime = NSDate()
session.dateCreated = NSDate()
session.dateModified = NSDate()
        
let sessionEvent = SessionEvent(actor: person, action: Action.LOGGED_IN(), object: app, eventTime: NSDate(), generated: session)
sessionEvent.edApp = app

let completion = {
(error: Error?, response: HTTPURLResponse?, data: Data?) -> Void in
	// Do something
}
                 
sensor.send(data: sessionEvent, completionHandler: completion)
```



### Objective-C
```objc
CLSOptions *options = [[CLSOptions alloc] init];
options.host = @"Your Event Store endpoint";
options.apiKey = @"Your Event Store API key or authorization token";
    
CLSClient *client = [[CLSClient alloc] initWithOptions:self.options];
CLSSensor *sensor = [[CLSSensor alloc] initWithIdentifier:@"Your sensor IRI" client:self.client];

CLSPerson *person = [[CLSPerson alloc] initWithIdentifier:@"Your person IRI"];
person.name = @"Your person's name";
    
CLSSoftwareApplication *app = [[CLSSoftwareApplication alloc] initWithIdentifier:@"Your app IRI"];
app.name = @"Your app's name";
    
CLSSession *session = [[CLSSession alloc] initWithIdentifier:@"Your session IRI" actor:person];
session.name = @"Your session name";
session.startedAtTime = [NSDate date];
session.dateCreated = [NSDate date];
session.dateModified = [NSDate date];
    
CLSSessionEvent *sessionEvent = [[CLSSessionEvent alloc] initWithActor:person action:[CLSAction LOGGED_IN] object:app eventTime:[NSDate date] generated:session];
sessionEvent.edApp = app;
    
void (^completionBlock)(NSError *, NSHTTPURLResponse*, NSData *) = ^(NSError* error, NSHTTPURLResponse *response, NSData *data){
        // Do something
};
    
[self.sensor sendWithData:sessionEvent completionHandler:completionBlock];
```
    
If sending data fails (perhaps due to a bad network connection or an expired authorization token), it will be saved to a queue to be sent with the next request.  

If the server responds with `400 Bad Request`, however, the data will not be added to the queue. In that case, check your usage of the sensor and ensure that you have the correct objects, properties, and values. For help with that, see the [Caliper Implementation Guide](http://www.imsglobal.org/caliper/caliperv1p0/ims-caliper-analytics-implementation-guide).