//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// The types of `DigitalResource`s supported in Caliper 1.0.
@objc(CLSDigitalResourceType) public class DigitalResourceType: NSObject {
    
    public class func ASSIGNABLE_DIGITAL_RESOURCE() -> String {
        return "http://purl.imsglobal.org/caliper/v1/AssignableDigitalResource"
    }
    
    public class func EPUB_CHAPTER() -> String {
        return "http://www.idpf.org/epub/vocab/structure/#chapter"
    }
    
    public class func EPUB_PART() -> String {
        return "http://www.idpf.org/epub/vocab/structure/#part"
    }
    
    public class func EPUB_SUB_CHAPTER() -> String {
        return "http://www.idpf.org/epub/vocab/structure/#subchapter"
    }
    
    public class func EPUB_VOLUME() -> String {
        return "http://www.idpf.org/epub/vocab/structure/#volume"
    }
    
    public class func FRAME() -> String {
        return "http://purl.imsglobal.org/caliper/v1/Frame"
    }
    
    public class func MEDIA_LOCATION() -> String {
        return "http://purl.imsglobal.org/caliper/v1/MediaLocation"
    }
    
    public class func MEDIA_OBJECT() -> String {
        return "http://purl.imsglobal.org/caliper/v1/MediaObject"
    }
    
    public class func READING() -> String {
        return "http://purl.imsglobal.org/caliper/v1/Reading"
    }
    
    public class func WEB_PAGE() -> String {
        return "http://purl.imsglobal.org/caliper/v1/WebPage"
    }
}
