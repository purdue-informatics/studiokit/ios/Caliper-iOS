//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

// Denotes the current status of a membership. The status applies to all roles.
@objc(CLSStatus) public class Status: NSObject {
    
    public var statusValue: String
    
    public init(statusValue: String) {
        self.statusValue = statusValue
    }
    
    public class func ACTIVE() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/status#Active"
    }
    
    public class func DELETED() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/status#Deleted"
    }
    
    public class func INACTIVE() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/status#Inactive"
    }
}
