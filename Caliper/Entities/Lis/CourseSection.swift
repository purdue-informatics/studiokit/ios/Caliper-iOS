//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/**
    A CourseSection is a way to represent a group of people associated
    with a course or class. These groups may include everyone in the
    class or course, or may be subsets of that whole group.
    CourseSections may have sub-sections (these are created as
    separate Group objects linked using the relationship). Examples of
    a CourseSection are Lecture, Laboratory, Studio, Seminar, etc.
    There may be several instances of a type of CourseSection e.g.,
    multiple lectures.

    A Caliper CourseSection provides a subset of the CourseSection
    properties specified in the IMS LTI 2.0 specification, which in
    turn, draws inspiration from the IMS LIS 1.0 specification.
*/

@objc(CLSCourseSection) public class CourseSection: CourseOffering {
    
    public var category: String?
    
    public override init(identifier: String) {
        super.init(identifier: identifier)
        self.type = EntityType.COURSE_SECTION()
    }
    
    override func dictionary() -> [String: Any] {
        var dictionary = super.dictionary()
        dictionary["category"] = category
        return dictionary
    }
    
}
