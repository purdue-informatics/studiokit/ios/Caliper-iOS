//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// IMS LTI/LIS roles.
@objc(CLSRole) public class Role: NSObject {
    
    public var roleValue: String
    
    public init(roleValue: String) {
        self.roleValue = roleValue
    }
    
    public class func LEARNER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership#Learner"
    }
    
    public class func EXTERNAL_LEARNER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Learner#ExternalLearner"
    }
    
    public class func GUEST_LEARNER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Learner#GuestLearner"
    }
    
    public class func LEARNER_INSTRUCTOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Learner#Instructor"
    }
    
    public class func LEARNER_LEARNER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Learner#Learner"
    }
    
    public class func NONCREDIT_LEARNER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Learner#NonCreditLearner"
    }
    
    public class func INSTRUCTOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership#Instructor"
    }
    
    public class func EXTERNAL_INSTRUCTOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Instructor#ExternalInstructor"
    }
    
    public class func GUEST_INSTRUCTOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Instructor#GuestInstructor"
    }
    
    public class func LECTURER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Instructor#Lecturer"
    }
    
    public class func PRIMARY_INSTRUCTOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Instructor#PrimaryInstructor"
    }
    
    public class func ADMINISTRATOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership#Administrator"
    }
    
    public class func ADMINISTRATOR_ADMINISTRATOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Administrator#Administrator"
    }
    
    public class func ADMINISTRATOR_DEVELOPER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Administrator#Developer"
    }
    
    public class func ADMINISTRATOR_SUPPORT() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Administrator#Support"
    }
    
    public class func ADMINISTRATOR_SYSTEM_ADMINISTRATOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Administrator#SystemAdministrator"
    }
    
    public class func ADMINISTRATOR_EXTERNAL_DEVELOPER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Administrator#ExternalSupport"
    }
    
    public class func ADMINISTRATOR_EXTERNAL_SUPPORT() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Administrator#ExternalDeveloper"
    }
    
    public class func ADMINISTRATOR_EXTERNAL_SYSTEM_ADMINISTRATOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Administrator#ExternalSystemAdministrator"
    }
    
    public class func CONTENT_DEVELOPER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership#ContentDeveloper"
    }
    
    public class func CONTENT_DEVELOPER_CONTENT_DEVELOPER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/ContentDeveloper#ContentDeveloper"
    }
    
    public class func CONTENT_DEVELOPER_LIBRARIAN() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/ContentDeveloper#Librarian"
    }
    
    public class func CONTENT_DEVELOPER_CONTENT_EXPERT() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/ContentDeveloper#ContentExpert"
    }
    
    public class func CONTENT_DEVELOPER_EXTERNAL_CONTENT_EXPERT() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/ContentDeveloper#ExternalContentExpert"
    }
    
    public class func MANAGER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership#Manager"
    }
    
    public class func MANAGER_AREA_MANAGER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Manager#AreaManager"
    }
    
    public class func MANAGER_COURSE_COORDINATOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Manager#CourseCoordinator"
    }
    
    public class func MANAGER_OBSERVER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Manager#Observer"
    }
    
    public class func MANAGER_EXTERNAL_OBSERVER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Manager#ExternalObserver"
    }
    
    public class func MEMBER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership#Member"
    }
    
    public class func MEMBER_MEMBER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Member#Member"
    }
    
    public class func MENTOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership#Mentor"
    }
    
    public class func MENTOR_MENTOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Mentor#Mentor"
    }
    
    public class func MENTOR_ADVISOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Mentor#Advisor"
    }
    
    public class func MENTOR_AUDITOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Mentor#Auditor"
    }
    
    public class func MENTOR_REVIEWER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Mentor#Reviewer"
    }
    
    public class func MENTOR_TUTOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Mentor#Tutor"
    }
    
    public class func MENTOR_LEARNING_FACILITATOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Mentor#LearningFacilitator"
    }
    
    public class func MENTOR_EXTERNAL_MENTOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Mentor#ExternalMentor"
    }
    
    public class func MENTOR_EXTERNAL_ADVISOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Mentor#ExternalAdvisor"
    }
    
    public class func MENTOR_EXTERNAL_AUDITOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Mentor#ExternalAuditor"
    }
    
    public class func MENTOR_EXTERNAL_REVIEWER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Mentor#ExternalReviewer"
    }
    
    public class func MENTOR_EXTERNAL_TUTOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Mentor#ExternalTutor"
    }
    
    public class func MENTOR_EXTERNAL_LEARNING_FACILITATOR() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/Mentor/ExternalLearningFacilitator"
    }
    
    public class func TEACHING_ASSISTANT() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership#TeachingAssistant"
    }
    
    public class func TEACHING_ASSISTANT_TEACHING_ASSISTANT() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/TeachingAssistant#TeachingAssistant"
    }
    
    public class func TEACHING_ASSISTANT_GRADER() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/TeachingAssistant#Grader"
    }
    
    public class func TEACHING_ASSISTANT_TEACHING_ASSISTANT_SECTION() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/TeachingAssistant#TeachingAssistantSection"
    }
    
    public class func TEACHING_ASSISTANT_TEACHING_ASSISTANT_SECTION_ASSOCIATION() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/TeachingAssistant#TeachingAssistantSectionAssociation"
    }
    
    public class func TEACHING_ASSISTANT_TEACHING_ASSISTANT_OFFERING() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/TeachingAssistant#TeachingAssistantOffering"
    }
    
    public class func TEACHING_ASSISTANT_TEACHING_ASSISTANT_TEMPLATE() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/TeachingAssistant#TeachingAssistantTemplate"
    }
    
    public class func TEACHING_ASSISTANT_TEACHING_ASSISTANT_GROUP() -> String {
        return "http://purl.imsglobal.org/vocab/lis/v2/membership/TeachingAssistant#TeachingAssistantGroup"
    }
}
