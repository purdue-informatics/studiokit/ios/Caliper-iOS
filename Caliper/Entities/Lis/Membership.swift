//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation


/**
    A Caliper Membership is used to define the relationship between
    objects that can have members and objects that can be members.
    Objects recognized as having members are CourseOffering,
    CourseSection and Group. Any Agent entity can be a member.
 */
 @objc(CLSMembership) public class Membership: Entity {
    
    public var member: Person?
    public var organization: Organization?
    public var roles = [Role]()
    public var status: Status?
    
    public override init(identifier: String) {
        super.init(identifier: identifier)
        self.type = EntityType.MEMBERSHIP()
    }
    
    override func dictionary() -> [String : Any] {
        let rolesArray = roles.map{$0.roleValue}
        
        var dictionary = super.dictionary()
        dictionary["member"] = member?.identifier
        dictionary["organization"] = organization?.identifier
        dictionary["roles"] = rolesArray
        dictionary["status"] = status?.statusValue
        return ["membership": dictionary]
    }
    
}
