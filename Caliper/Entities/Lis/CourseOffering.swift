//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/**
    A CourseOffering is the occurrence of a course in a specific term,
    semester, etc. A Caliper CourseOffering provides a subset of the
    CourseOffering properties specified in the IMS LTI 2.0 specification,
    which in turn, draws inspiration from the IMS LIS 1.0 specification.
*/

@objc(CLSCourseOffering) public class CourseOffering: Course {
    
    public var courseNumber: String?
    public var academicSession: String?
    
    public override init(identifier: String) {
        super.init(identifier: identifier)
        self.type = EntityType.COURSE_OFFERING()
    }
    
    override func dictionary() -> [String: Any] {
        var dictionary = super.dictionary()
        dictionary["courseNumber"] = courseNumber
        dictionary["academicSession"] = academicSession
        return dictionary
    }
    
}
