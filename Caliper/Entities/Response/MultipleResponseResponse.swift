//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/**
    Representation of a response to a multiple choice question that permits one or more
    options to be selected.
 */
@objc(CLSMultipleResponseResponse) public class MultipleResponseResponse: Response {
    
    public var values = [String]()
    
    public override init(identifier: String, startedAt: Date) {
        super.init(identifier: identifier, startedAt: startedAt)
        self.type = ResponseType.MULTIPLERESPONSE()
    }
    
    override func dictionary() -> [String : Any] {
        var dictionary = super.dictionary()
        dictionary["values"] = values
        return dictionary
    }
}
