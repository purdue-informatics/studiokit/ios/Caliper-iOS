//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// Represents a response to a multiple choice question that permits a single option to be selected.
@objc(CLSMultipleChoiceResponse) public class MultipleChoiceResponse: Response {
    
    public var value: String?
    
    public override init(identifier: String, startedAt: Date) {
        super.init(identifier: identifier, startedAt: startedAt)
        self.type = ResponseType.MULTIPLECHOICE()
    }
    
    override func dictionary() -> [String : Any] {
        var dictionary = super.dictionary()
        dictionary["value"] = value
        return dictionary
    }
}
