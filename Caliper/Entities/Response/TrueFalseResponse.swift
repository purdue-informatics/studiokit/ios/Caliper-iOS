//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/**
    Represents response to a multiple choice question that limits options to either 'true or false',
    'agree or disagree', etc.
*/
@objc(CLSTrueFalseResponse) public class TrueFalseResponse: Response {
    
    public var value: String
    
    public init(identifier: String, startedAt: Date, value: String) {
        self.value = value
        super.init(identifier: identifier, startedAt: startedAt)
        self.type = ResponseType.TRUEFALSE()
    }
    
    override func dictionary() -> [String : Any] {
        var dictionary = super.dictionary()
        dictionary["value"] = value
        return dictionary
    }
}
