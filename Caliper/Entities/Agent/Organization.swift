//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/**
    A collection of people organized together into a community or other social, commercial or political structure.
    The group has some common purpose or reason for existence which goes beyond the set of people belonging to it and can act as an Agent.
    Organizations are often decomposable into hierarchical structures.
 */
@objc(CLSOrganization) public class Organization: Entity {
    
    public var subOrganizationOf: Organization?
    
    public override init(identifier: String) {
        super.init(identifier: identifier)
        self.type = EntityType.ORGANIZATION()
    }
    
    override func dictionary() -> [String: Any] {
        var dictionary = super.dictionary()
        dictionary["subOrganizationOf"] = subOrganizationOf?.dictionary()
        return dictionary
    }
}
