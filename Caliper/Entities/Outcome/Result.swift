//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/**
    Representation of a Result. `Result`s are generated as part of an interaction
    represented by an `OutcomeEvent`.
*/
@objc(CLSResult) public class Result: Entity {
    
    public var assignable: DigitalResource?
    public var actor: Agent?
    public var normalScore: Double = 0.0
    public var penaltyScore: Double = 0.0
    public var extraCreditScore: Double = 0.0
    public var totalScore: Double = 0.0
    public var curvedTotalScore: Double = 0.0
    public var curveFactor: Double = 0.0
    public var comment: String?
    public var scoredBy: Agent
    
    public init(identifier: String, scoredBy: Agent) {
        self.scoredBy = scoredBy
        super.init(identifier: identifier)
        self.type = EntityType.RESULT()
    }
    
    override func dictionary() -> [String : Any] {
        var dictionary = super.dictionary()
        dictionary["assignable"] = assignable
        dictionary["actor"] = actor
        dictionary["normalScore"] = normalScore
        dictionary["penaltyScore"] = penaltyScore
        dictionary["extraCreditScore"] = extraCreditScore
        dictionary["totalScore"] = totalScore
        dictionary["curvedTotalScore"] = curvedTotalScore
        dictionary["curveFactor"] = curveFactor
        dictionary["comment"] = comment
        dictionary["scoredBy"] = scoredBy
        return dictionary
    }
}
