//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// The types of `MediaObject` supported in Caliper 1.0.
@objc(CLSMediaObjectType) public class MediaObjectType: NSObject {
    
    public class func AUDIO_OBJECT() -> String {
        return "http://purl.imsglobal.org/caliper/v1/AudioObject"
    }
    
    public class func IMAGE_OBJECT() -> String {
        return "http://purl.imsglobal.org/caliper/v1/ImageObject"
    }
    
    public class func VIDEO_OBJECT() -> String {
        return "http://purl.imsglobal.org/caliper/v1/VideoObject"
    }
}
