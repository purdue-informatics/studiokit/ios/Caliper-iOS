//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// An audio object used in a web page or app.
@objc(CLSAudioObject) public class AudioObject: MediaObject {
    
    public var volumeMin: String?
    public var volumeMax: String?
    public var volumeLevel: String?
    public var muted = false
    
    public override init(identifier: String) {
        super.init(identifier: identifier)
        self.type = MediaObjectType.AUDIO_OBJECT()
    }
    
    override func dictionary() -> [String : Any] {
        var dictionary = super.dictionary()
        dictionary["volumeMin"] = volumeMin
        dictionary["volumeMax"] = volumeMax
        dictionary["volumeLevel"] = volumeLevel
        dictionary["muted"] = muted
        return dictionary
    }
}
