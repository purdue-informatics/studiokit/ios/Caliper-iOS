//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

@objc(CLSSession) public class Session: Entity {
    
    public var actor: Agent
    public var startedAtTime: Date?
    public var endedAtTime: Date?
    public var duration: TimeInterval?
    
    public init(identifier: String, actor: Agent) {
        self.actor = actor
        super.init(identifier: identifier)
        self.type = EntityType.SESSION()
        
    }
    
    override func dictionary() -> [String : Any] {
        var dictionary = super.dictionary()
        dictionary["startedAtTime"] = startedAtTime?.iso()
        dictionary["endedAtTime"] = endedAtTime?.iso()
        dictionary["actor"] = actor.dictionary()
        dictionary["duration"] = duration?.xsdDuration()
        return dictionary
    }
}
