//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/**
    The Entity interface provides the minimal set of properties and behaviors required of a Caliper
    Entity.  Analogous to a schema.org Thing and a JSON-LD node in a graph.
    For an Entity to be linkable, dereferencing the identifier
    should result in a representation of the node.
 */
@objc(CLSEntity) public class Entity: BaseObject {
    
    /**
        The JSON-LD context provides a mapping of terms to IRIs.  The identifier
        should be expressed as a unique IRI in conformance with the JSON-LD specification.
     */
    public var context: String = Context().CONTEXT

    /**
        Each Entity (or node in the graph as defined by JSON-LD) requires an identifier.
        The **identifier should be expressed as a unique IRI** in conformance with the
        JSON-LD specification.
     */
    public var identifier: String
    
    /**
        Specifies the type of Entity or node in the graph as defined by JSON-LD.
        For list of available types, see `EntityType`.
     */
    public var type: String
    
    public var name: String?
    public var entityDescription: String?
    public var extensions = [String: String]()
    public var dateCreated: Date?
    public var dateModified: Date?
    
    public init(identifier: String) {
        self.identifier = identifier
        self.type = ""
        super.init()
    }

    var baseDictionary: [String: Any] {
        get {
            return [
                "@context": context,
                "@id": identifier as Any,
                "@type": type as Any,
                "name": name as Any,
                "description": entityDescription as Any,
                "extensions": extensions,
                "dateCreated": dateCreated?.iso() as Any,
                "dateModified": dateModified?.iso() as Any
            ]
        }
    }
    
    override func dictionary() -> [String: Any] {
        return baseDictionary
    }
}



