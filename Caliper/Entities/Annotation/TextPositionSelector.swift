//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/**
    A selector which describes a range of text based on its start and end positions.
    Defined by: http://www.w3.org/ns/oa#d4e667
 */

@objc(CLSTextPositionSelector) public class TextPositionSelector: NSObject {
    
    public var start: String
    public var end: String
    
    public init(start: String, end: String) {
        self.start = start
        self.end = end
    }
    
    func dictionary() -> [String: String?] {
        return [
            "start": start,
            "end": end
        ]
    }
}
