//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/**
    Base type for all annotation types. Direct sub-types, such as
    Highlight, Attachment, etc, are specified in the Caliper
    Annotation Metric Profile.
 */
@objc(CLSAnnotation) public class Annotation: Entity {
    
    public var annotated: DigitalResource
    
    public init(identifier: String, annotated: DigitalResource) {
        self.annotated = annotated
        super.init(identifier: identifier)
    }
    
    override func dictionary() -> [String : Any] {
        var dictionary = super.dictionary()
        dictionary["annotated"] = annotated.identifier as Any
        return dictionary
    }
    
}
