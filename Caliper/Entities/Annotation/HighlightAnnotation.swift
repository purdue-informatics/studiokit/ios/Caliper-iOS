//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
import Foundation

@objc(CLSHightlightAnnotation) public class HighlightAnnotation: Annotation {
    
    public var selection: TextPositionSelector
    public var selectionText: String

    public init(identifier: String, annotated: DigitalResource, selection: TextPositionSelector, selectionText: String) {
        self.selection = selection
        self.selectionText = selectionText
        super.init(identifier: identifier, annotated: annotated)
        self.type = AnnotationType.HIGHLIGHT_ANNOTATION()
    }
    
    override func dictionary() -> [String : Any] {
        var dictionary = super.dictionary()
        dictionary["selection"] = selection.dictionary()
        dictionary["selectionText"] = selectionText
        return dictionary
    }
    
}
