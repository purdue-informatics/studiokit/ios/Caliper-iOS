//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/**
    Assignable Digital Resource, a part of the Assignable metric profile
*/
@objc(CLSAssignableDigitalResource) public class AssignableDigitalResource: DigitalResource {
    
    public var dateToActivate: Date?
    public var dateToShow: Date?
    public var dateToStartOn: Date?
    public var dateToSubmit: Date?
    public var maxAttempts: Int = 0
    public var maxSubmits: Int = 0
    public var maxScore: Double = 0.0
    
    public override init(identifier: String) {
        super.init(identifier: identifier)
        self.type = DigitalResourceType.ASSIGNABLE_DIGITAL_RESOURCE()
    }
    
    override func dictionary() -> [String : Any] {
        var dictionary = super.dictionary()
        dictionary["dateToActivate"] = dateToActivate
        dictionary["dateToShow"] = dateToShow
        dictionary["dateToStartOn"] = dateToStartOn
        dictionary["dateToSubmit"] = dateToSubmit
        dictionary["maxAttempts"] = maxAttempts
        dictionary["maxSubmits"] = maxSubmits
        dictionary["maxScore"] = maxScore
        
        return dictionary
    }
}
