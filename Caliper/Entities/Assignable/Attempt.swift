//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/**
    Representation of an Attempt. Attempts are generated as part of or
    are the object of an interaction represented by an AssignableEvent.
*/
@objc(CLSAttempt) public class Attempt: Entity {
    
    public var assignable: DigitalResource?
    public var actor: Agent
    public var count: Int = 0
    public var startedAtTime: Date
    public var endedAtTime: Date?
    public var duration: TimeInterval?
    
    public init(identifier: String, actor: Agent, startedAtTime: Date) {
        self.actor = actor
        self.startedAtTime = startedAtTime
        super.init(identifier: identifier)
        self.type = EntityType.ATTEMPT()
    }
    
    override func dictionary() -> [String : Any] {
        var dictionary = super.dictionary()
        dictionary["assignable"] = assignable
        dictionary["actor"] = actor
        dictionary["count"] = count
        dictionary["startedAtTime"] = startedAtTime
        dictionary["endedAtTime"] = endedAtTime
        dictionary["duration"] = duration?.xsdDuration()
        return dictionary
    }
}
