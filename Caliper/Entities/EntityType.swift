//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// The entity types supported in Caliper 1.0.
@objc(CLSEntityType) public class EntityType: NSObject {
    
    public class func ANNOTATION() -> String {
        return "http://purl.imsglobal.org/caliper/v1/Annotation"
    }
    
    public class func ATTEMPT() -> String {
        return "http://purl.imsglobal.org/caliper/v1/Attempt"
    }

    public class func COURSE_OFFERING() -> String {
        return "http://purl.imsglobal.org/caliper/v1/lis/CourseOffering"
    }

    public class func COURSE_SECTION() -> String {
        return "http://purl.imsglobal.org/caliper/v1/lis/CourseSection"
    }
    
    public class func DIGITAL_RESOURCE() -> String {
        return "http://purl.imsglobal.org/caliper/v1/DigitalResource"
    }
    
    public class func ENTITY() -> String {
        return "http://purl.imsglobal.org/caliper/v1/Entity"
    }
    
    public class func GROUP() -> String {
        return "http://purl.imsglobal.org/caliper/v1/lis/Group"
    }
    
    public class func LEARNING_OBJECTIVE() -> String {
        return "http://purl.imsglobal.org/caliper/v1/LearningObjective"
    }
    
    public class func MEMBERSHIP() -> String {
        return "http://purl.imsglobal.org/caliper/v1/lis/Membership"
    }
    
    public class func PERSON() -> String {
        return "http://purl.imsglobal.org/caliper/v1/lis/Person"
    }
    
    public class func ORGANIZATION() -> String {
        return "http://purl.imsglobal.org/caliper/v1/w3c/Organization"
    }
    
    public class func RESPONSE() -> String {
        return "http://purl.imsglobal.org/caliper/v1/Response"
    }
    
    public class func RESULT() -> String {
        return "http://purl.imsglobal.org/caliper/v1/Result"
    }
    
    public class func SESSION() -> String {
        return "http://purl.imsglobal.org/caliper/v1/Session"
    }
    
    public class func SOFTWARE_APPLICATION() -> String {
        return "http://purl.imsglobal.org/caliper/v1/SoftwareApplication"
    }
    
    public class func VIEW() -> String {
        return "http://purl.imsglobal.org/caliper/v1/View"
    }
}
