//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/**
    Concrete implementation of a generic digital resource.  Analogous to schema.org's CreativeWork.
    (see https://schema.org/CreativeWork).
*/
@objc(CLSDigitalResource) public class DigitalResource: Entity {
    
    public var objectTypes = [String]()
    public var learningObjectives = [LearningObjective]()
    public var keywords = [String]()
    public var isPartOf: DigitalResource?
    public var datePublished: Date?
    public var version: String?
    
    public override init(identifier: String) {
        super.init(identifier: identifier)
        self.type = EntityType.DIGITAL_RESOURCE()
    }
    
    override func dictionary() -> [String : Any] {
        var dictionary = super.dictionary()
        dictionary["objectType"] = objectTypes
        dictionary["alignedLearningObjective"] = learningObjectives
        dictionary["keywords"] = keywords
        dictionary["isPartOf"] = isPartOf?.dictionary()
        dictionary["datePublished"] = datePublished?.iso()
        dictionary["version"] = version
        return dictionary
    }
}
