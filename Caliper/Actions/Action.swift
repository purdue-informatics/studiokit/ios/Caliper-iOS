//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// The types of actions supported in Caliper 1.0
@objc(CLSAction) public class Action: NSObject {
    
    public class func ABANDONED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Abandoned"
    }
    
    public class func ACTIVATED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Activated"
    }
    
    public class func ATTACHED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Attached"
    }
    
    public class func BOOKMARKED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Bookmarked"
    }
    
    public class func CHANGED_RESOLUTION() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#ChangedResolution"
    }
    
    public class func CHANGED_SIZE() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#ChangedSize"
    }
    
    public class func CHANGED_SPEED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#ChangedSpeed"
    }
    
    public class func CHANGED_VOLUME() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#ChangedVolume"
    }
    
    public class func CLASSIFIED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Classified"
    }
    
    public class func CLOSED_POPOUT() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#ClosedPopout"
    }
    
    public class func COMMENTED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Commented"
    }
    
    public class func COMPLETED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Completed"
    }
    
    public class func DEACTIVATED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Deactivated"
    }
    
    public class func DESCRIBED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Described"
    }
    
    public class func DISLIKED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Disliked"
    }
    
    public class func DISABLED_CLOSED_CAPTIONING() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#DisabledClosedCaptioning"
    }
    
    public class func ENABLED_CLOSED_CAPTIONING() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#EnabledClosedCaptioning"
    }
    
    public class func ENDED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Ended"
    }
    
    public class func ENTERED_FULLSCREEN() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#EnteredFullscreen"
    }
    
    public class func EXITED_FULLSCREEN() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#ExitedFullscreen"
    }
    
    public class func FORWARDED_TO() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#ForwardedTo"
    }
    
    public class func GRADED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Graded"
    }
    
    public class func HID() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Hid"
    }
    
    public class func HIGHLIGHTED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Highlighted"
    }
    
    public class func JUMPED_TO() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#JumpedTo"
    }
    
    public class func IDENTIFIED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Identified"
    }
    
    public class func LIKED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Liked"
    }
    
    public class func LINKED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Linked"
    }
    
    public class func LOGGED_IN() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#LoggedIn"
    }
    
    public class func LOGGED_OUT() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#LoggedOut"
    }
    
    public class func MUTED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Muted"
    }
    
    public class func NAVIGATED_TO() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#NavigatedTo"
    }
    
    public class func OPENED_POPOUT() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#OpenedPopout"
    }
    
    public class func PAUSED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Paused"
    }
    
    public class func RANKED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Ranked"
    }
    
    public class func QUESTIONED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Questioned"
    }
    
    public class func RECOMMENDED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Recommended"
    }
    
    public class func REPLIED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Replied"
    }
    
    public class func RESTARTED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Restarted"
    }
    
    public class func RESUMED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Resumed"
    }
    
    public class func REVIEWED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Reviewed"
    }
    
    public class func REWOUND() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Rewound"
    }
    
    public class func SEARCHED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Searched"
    }
    
    public class func SHARED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Shared"
    }
    
    public class func SHOWED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Showed"
    }
    
    public class func SKIPPED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Skipped"
    }
    
    public class func STARTED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Started"
    }
    
    public class func SUBMITTED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Submitted"
    }
    
    public class func SUBSCRIBED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Subscribed"
    }
    
    public class func TAGGED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Tagged"
    }
    
    public class func TIMED_OUT() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#TimedOut"
    }
    
    public class func VIEWED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Viewed"
    }
    
    public class func UNMUTED() -> String {
        return "http://purl.imsglobal.org/vocab/caliper/v1/action#Unmuted"
    }
}
