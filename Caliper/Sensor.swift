//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// A Caliper sensor for sending events and entities to Caliper endpoints.
@objc(CLSSensor) public class Sensor: NSObject {
    
    /// The Sensor identifier must be a valid IRI.
    public var identifier: String
    
    /// The Caliper Client.
    public var client: Client
    
    /// Create a Sensor.
    public init(identifier: String, client: Client) {
        self.identifier = identifier
        self.client = client
    }
    
    /**
        Send a single `Entity` describes to a target event store.
     
        - Parameters
            - data: The entity to send.
            - completionHandler: A block that will be executed after the entity has been processed. It gives back an error if there was one. If the server sends back a response and/or data, those will be represented also.
     */
    public func describe(data: Entity, completionHandler: @escaping (_ error: Error?, _ response: HTTPURLResponse?, _ data: Data?, _ caliperData: Data?) -> Void) {
        client.describe(sensor: self, data: data, completionHandler: completionHandler)
    }
    
    /**
        Send a batch of `Entity`s describes to a target event store.
     
        - Parameters:
            - data: The `Entity`s to send.
            - completionHandler: A block that will be executed after the `Entity`s have been processed.
            It gives back an error if there was one. If the server sends back a response and/or data,
            those will be represented also.
     */
    @objc(describeBatch: completion:)
    public func describe(data: [Entity], completionHandler: @escaping (_ error: Error?, _ response: HTTPURLResponse?, _ data: Data?, _ caliperData: Data?) -> Void) {
        client.describe(sensor: self, data: data, completionHandler: completionHandler)
    }
    
    /**
        Send a single `Event` describes to a target event store.
     
        - Parameter
            - data: The entity to send.
            - Parameter completionHandler: A block that will be executed after the event has been processed. It gives back an error if there was one. If the server sends back a response and/or data, those will be represented also.
     */
    public func send(data: Event, completionHandler: @escaping (_ error: Error?, _ response: HTTPURLResponse?, _ data: Data?, _ caliperData: Data?) -> Void) {
        client.send(sensor: self, data: data, completionHandler: completionHandler)
    }
    
    /**
        Send a batch of `Event`s describes to a target event store.
     
        - Parameters
            - data: The `Event`s to send.
            - completionHandler: A block that will be executed after the `Event`s have been processed.
            It gives back an error if there was one. If the server sends back a response and/or data,
            those will be represented also.
     */
    @objc(sendBatch: completion:)
    public func send(data: [Event], completionHandler: @escaping (_ error: Error?, _ response: HTTPURLResponse?, _ data: Data?, _ caliperData: Data?) -> Void) {
        client.send(sensor: self, data: data, completionHandler: completionHandler)
    }
    
}
