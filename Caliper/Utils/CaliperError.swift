//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

public enum CaliperError: Error {
    /**
        The data provided to the sensor was not in the correct format.
    */
    case invalidData
    
    /// The value provided for `Options.host` was invalid.
    case invalidHost(host: String)
    
    /**
        Unable to send to store. This could be due to a variety of reasons, such as a server error,
        invalid authorization tokens, or other connection issues.
     
    */
    case unableToSendToStore(host: String)
    
    /**
        Unable to send to store because the server gave a `401 Unauthorized`.
    */
    case unauthorized
    
    /**
        Unable to send to store because the client was offline or had a bad connection.
    */
    case offline
    case other(message: String)
}

extension CaliperError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .invalidHost(let host):
            return NSLocalizedString("Host \(host) is invalid.", comment: "")
        case .invalidData:
            return NSLocalizedString("Caliper data was not in the correct format.", comment: "")
        case .unableToSendToStore:
            return NSLocalizedString("Unable to save to event store.", comment: "")
        case .unauthorized:
            return NSLocalizedString("Unable to send to event store. Received a 401 Unauthorized.", comment: "")
        case .offline:
            return NSLocalizedString("Unable to send to event store because of the Internet connection.", comment: "")
        case .other(let message):
            return NSLocalizedString(message, comment: "")
        }
    }
    
    public var errorReason: String? {
        switch self {
        case .unauthorized:
            return NSLocalizedString("401", comment: "")
        case .offline:
            return NSLocalizedString("Offline", comment: "")
        default: break
        }
        return nil
    }
}
