//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

extension TimeInterval {
    
    func xsdDuration() -> String {
        let calendar = Calendar(identifier: .gregorian)
        
        // Create empty date components
        var components = DateComponents()
        
        // Create an empty date (January 1, 1 at midnight)
        let emptyDate = calendar.date(from: components)
        
        // Add the duration to the empty date
        let durationDate = Date(timeInterval: self, since: emptyDate!)
        
        // Update the components with the new date
        components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: durationDate)
        
        // Years, months, and days are 1 based, not 0 based, so subtract 1
        // I.e. if the day is 24, then 23 days have elapsed
        let year = components.year! - 1
        let month = components.month! - 1
        let day = components.day! - 1
        let hour = components.hour!
        let minute = components.minute!
        let second = components.second!
        
        return "P\(year)Y\(month)M\(day)DT\(hour)H\(minute)M\(second)S"
    }
}
