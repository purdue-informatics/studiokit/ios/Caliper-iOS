//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

extension Date {
        
    func iso() -> String {
        if #available(iOS 10, *) {
            let dateFormatter = ISO8601DateFormatter()
            return dateFormatter.string(from: self)
        }
        else {
            // Via http://stackoverflow.com/q/16254575
            let dateFormatter = DateFormatter()
            let timeZone = TimeZone(abbreviation: "UTC")
            let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
            dateFormatter.locale = enUSPosixLocale
            dateFormatter.timeZone = timeZone
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
            return dateFormatter.string(from: self)
        }
    }
}




