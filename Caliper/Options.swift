//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// Options for registering a Caliper endpoint with the Caliper sensor.
@objc(CLSOptions) public class Options: NSObject {
    
    /// The Caliper web API endpoint.
    public var host = Defaults.HOST.rawValue
    
    /// The Caliper web API key.
    public var apiKey: String?
    
    /**
        Log the `Event` or `Entity`'s JSON. Useful for debugging.
        Default is `false`
    */
    public var logJSON = false
}
