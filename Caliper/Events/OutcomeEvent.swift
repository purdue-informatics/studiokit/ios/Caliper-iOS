//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// Event raised when actions related to an outcome are performed.
@objc(CLSOutcomeEvent) public class OutcomeEvent: Event {
    
    public init(actor: Agent, action: String, object: Attempt, eventTime: Date, target: AssignableDigitalResource) {
        super.init(actor: actor, action: action, object: object, eventTime: eventTime)
        
        self.target = target
        
        self.type = EventType.OUTCOME()
        self.supportedActions = [
            Action.GRADED()
        ]
    }
}
