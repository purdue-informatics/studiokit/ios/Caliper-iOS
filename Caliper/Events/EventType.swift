//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

@objc(CLSEventType) public class EventType: NSObject {
    
    public class func ANNOTATION() -> String {
        return "http://purl.imsglobal.org/caliper/v1/AnnotationEvent"
    }
    
    public class func ASSESSMENT() -> String {
        return "http://purl.imsglobal.org/caliper/v1/AssessmentEvent"
    }
    
    public class func ASSESSMENT_ITEM() -> String {
        return "http://purl.imsglobal.org/caliper/v1/AssessmentItemEvent"
    }
    
    public class func ASSIGNABLE() -> String {
        return "http://purl.imsglobal.org/caliper/v1/AssignableEvent"
    }
    
    public class func EVENT() -> String {
        return "http://purl.imsglobal.org/caliper/v1/Event"
    }
    
    public class func MEDIA() -> String {
        return "http://purl.imsglobal.org/caliper/v1/MediaEvent"
    }
    
    public class func NAVIGATION() -> String {
        return "http://purl.imsglobal.org/caliper/v1/NavigationEvent"
    }
    
    public class func OUTCOME() -> String {
        return "http://purl.imsglobal.org/caliper/v1/OutcomeEvent"
    }
    
    public class func READING() -> String {
        return "http://purl.imsglobal.org/caliper/v1/ReadingEvent"
    }
    
    public class func SESSION() -> String {
        return "http://purl.imsglobal.org/caliper/v1/SessionEvent"
    }
    
    public class func VIEW() -> String {
        return "http://purl.imsglobal.org/caliper/v1/ViewEvent"
    }
}
