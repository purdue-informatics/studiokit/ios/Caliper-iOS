//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

@objc(CLSSessionEvent) public class SessionEvent: Event {
    
    /**
     Create a `SessionEvent`.
     
     _Notes:_
     
     `object` is a `SoftwareApplication` for a `LOGGED_IN` action, and a `Session` for a `LOGGED_OUT` or `TIMED_OUT` action.
     
     `generated` is required for a `LOGGED_IN` action, but not for a `LOGGED_OUT` or `TIMED_OUT` action.
     */
    public init(actor: Agent, action: String, object: Entity, eventTime: Date, generated: Session? = nil) {
        super.init(actor: actor, action: action, object: object, eventTime: eventTime)
        self.generated = generated
        self.type = EventType.SESSION()
        self.supportedActions = [
            Action.LOGGED_IN(),
            Action.LOGGED_OUT(),
            Action.TIMED_OUT()
        ]
    }
}
