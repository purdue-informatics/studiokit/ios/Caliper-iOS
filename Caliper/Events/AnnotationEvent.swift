//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
import Foundation

/// Event raised when an actor annotates a resource.
@objc(CLSAnnotationEvent) public class AnnotationEvent: Event {
    
    public init(actor: Agent, action: String, object: DigitalResource, eventTime: Date, generated: Annotation) {
        super.init(actor: actor, action: action, object: object, eventTime: eventTime)
        self.generated = generated
        
        self.type = EventType.ANNOTATION()
        self.supportedActions = [
            Action.ATTACHED(),
            Action.BOOKMARKED(),
            Action.CLASSIFIED(),
            Action.COMMENTED(),
            Action.DESCRIBED(),
            Action.DISLIKED(),
            Action.HIGHLIGHTED(),
            Action.IDENTIFIED(),
            Action.LIKED(),
            Action.LINKED(),
            Action.RANKED(),
            Action.QUESTIONED(),
            Action.RECOMMENDED(),
            Action.REPLIED(),
            Action.SHARED(),
            Action.SUBSCRIBED(),
            Action.TAGGED()
        ]
    }
}
