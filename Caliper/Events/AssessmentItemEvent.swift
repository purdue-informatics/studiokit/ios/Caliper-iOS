//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// Event raised when an actor interacts with an assessment item resource.
@objc(CLSAssessmentItemEvent) public class AssessmentItemEvent: Event {
    
    /**
     Create an `AssessmentItemEvent`.
     
     _Notes:_
     
     `object` is a `AssignableDigitalResource` for the `STARTED`, `SKIPPED`, and `VIEWED` actions, and a `Attempt` for the other actions.
     
     `target` is not required for the `STARTED`, `SKIPPED`, and `VIEWED` actions, but is required for the other actions and is a `AssignableDigitalResource`.
     
     `generated` is required for a `STARTED` action, and is an `Attempt`. It is also required for a `Completed` action, and is a `Response`.
     */
    public init(actor: Agent, action: String, object: Entity, eventTime: Date, target: AssignableDigitalResource? = nil, generated: Entity? = nil) {
        super.init(actor: actor, action: action, object: object, eventTime: eventTime)
        
        self.type = EventType.ASSESSMENT_ITEM()
        self.supportedActions = [
            Action.STARTED(),
            Action.COMPLETED(),
            Action.SKIPPED(),
            Action.REVIEWED(),
            Action.VIEWED()
        ]
    }
}
