//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// Event raised when an actor navigates from one resource to another.
@objc(CLSNavigationEvent) public class NavigationEvent: Event {
    
    public var fromResource: DigitalResource?
    
    public override init(actor: Agent, action: String, object: Entity, eventTime: Date) {
        super.init(actor: actor, action: action, object: object, eventTime: eventTime)
        
        self.type = EventType.NAVIGATION()
        self.supportedActions = [
            Action.NAVIGATED_TO()
        ]
    }
    
    override func dictionary() -> [String: Any] {
        var dictionary = super.dictionary()
        dictionary["navigatedFrom"] = fromResource
        return dictionary
    }

}
