//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// Event raised when an actor interacts with a media resource.
@objc(CLSMediaEvent) public class MediaEvent: Event {
    
    /**
     Create an `MediaEvent`.
     
     _Notes:_
     
     `target` is required for the `STARTED`, `REWOUND`, `RESUMED`, `FORWARDED_TO`, `PAUSED`, and `JUMPED_TO` actions.
     */
    public init(actor: Agent, action: String, object: MediaObject, eventTime: Date, target: MediaLocation? = nil) {
        super.init(actor: actor, action: action, object: object, eventTime: eventTime)
        self.target = target
        
        self.type = EventType.MEDIA()
        self.supportedActions = [
            Action.OPENED_POPOUT(),
            Action.CLOSED_POPOUT(),
            Action.EXITED_FULLSCREEN(),
            Action.ENTERED_FULLSCREEN(),
            Action.CHANGED_SIZE(),
            Action.CHANGED_RESOLUTION(),
            Action.STARTED(),
            Action.REWOUND(),
            Action.RESUMED(),
            Action.FORWARDED_TO(),
            Action.PAUSED(),
            Action.JUMPED_TO(),
            Action.ENDED(),
            Action.CHANGED_SPEED(),
            Action.UNMUTED(),
            Action.MUTED(),
            Action.CHANGED_VOLUME(),
            Action.DISABLED_CLOSED_CAPTIONING(),
            Action.ENABLED_CLOSED_CAPTIONING()
        ]
    }
}
