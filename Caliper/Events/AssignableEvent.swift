//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// Event raised when an actor interacts with an assignable resource.
@objc(CLSAssignableEvent) public class AssignableEvent: Event {
    
    
    /**
     Create an `AssignableEvent`.
     
     _Notes:_
     
     `object` is an `Attempt` for the `COMPLETED`, `REVIEWED`, and `ABANDONED` actions, and a `AssignableDigitalResource` for the other actions.
     
     `target` is required for for the `COMPLETED`, `REVIEWED`, and `ABANDONED` actions, and is a `AssignableDigitalResource`.
     
     `generated` is required for a `STARTED` action, and is an `Attempt`.
     */
    public init(actor: Agent, action: String, object: Entity, eventTime: Date, target: AssignableDigitalResource? = nil, generated: Attempt? = nil) {
        super.init(actor: actor, action: action, object: object, eventTime: eventTime)
        self.generated = generated
        
        self.type = EventType.ASSIGNABLE()
        self.supportedActions = [
            Action.ABANDONED(),
            Action.ACTIVATED(),
            Action.COMPLETED(),
            Action.DEACTIVATED(),
            Action.HID(),
            Action.REVIEWED(),
            Action.SHOWED(),
            Action.STARTED()
        ]
    }
}
