//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

@objc(CLSClient) public class Client: NSObject {
    
    public var identifier: String?
    var requestor: Requestor
    public var options: Options {
        didSet {
            requestor.options = options
        }
    }
    
    public init(options: Options) {
        self.options = options
        self.requestor = Requestor(options: options)
        super.init()
    }
    
    
    @nonobjc
    func describe(sensor: Sensor, data: Entity, completionHandler: @escaping (_ error : Error?, _ response: HTTPURLResponse?, _ data: Data?, _ caliperData: Data?) -> ()) {
        let dataArray = [data]
        describe(sensor: sensor, data: dataArray, completionHandler: completionHandler)
    }

    @nonobjc
    func describe(sensor: Sensor, data: [Entity], completionHandler: @escaping (_ error : Error?, _ response: HTTPURLResponse?, _ data: Data?, _ caliperData: Data?) -> ()) {
        setHandlers(completionHandler: completionHandler)
        requestor.send(sensor: sensor, data:data)
    }
    
    @nonobjc
    func send(sensor: Sensor, data: Event, completionHandler: @escaping (_ error : Error?, _ response: HTTPURLResponse?, _ data: Data?, _ caliperData: Data?) -> ()) {
        
        let dataArray = [data]
        send(sensor: sensor, data: dataArray, completionHandler: completionHandler)
    }
    
    func setHandlers(completionHandler:@escaping (_ error: Error?, _ response: HTTPURLResponse?, _ data: Data?, _ caliperData: Data?) -> ()) {
        requestor.completionHandler = completionHandler
    }
    
    @nonobjc
    func send(sensor: Sensor, data: [Event], completionHandler: @escaping (_ error: Error?, _ response: HTTPURLResponse?, _ data: Data?, _ caliperData: Data?) -> ()) {
        setHandlers(completionHandler: completionHandler)
        
        requestor.send(sensor: sensor, data: data)
    }
}
