//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

class Envelope {
    
    var sensor: Sensor
    var sendTime: Date
    var data = [BaseObject]()
    
    init(sensor: Sensor, sendTime: Date, data: [BaseObject]) {
        self.sensor = sensor
        self.sendTime = sendTime
        self.data = data
    }
    
    func envelopeDictionary() -> [String: Any] {
        let envelopeData = data.map {$0.dictionary()}
        
        let dictionary: [String: Any] = [
            "sensor": sensor.identifier,
            "sendTime": sendTime.iso(),
            "data": envelopeData 
        ]
        return dictionary
    }
}
