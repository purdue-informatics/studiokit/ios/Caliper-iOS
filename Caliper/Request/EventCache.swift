//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation


// Inspired by sample project from Objc.io: https://talk.objc.io/episodes/S01E25-network-caching
class EventCache {
    
    class func saveEvent(data: Data) -> String? {
        
        // If the directory already exists, then "creating" the directory succeeds
        // Otherwise the directory will be created
        do {
            try FileManager.default.createDirectory(at: baseUrl(), withIntermediateDirectories: true, attributes: nil)
        }
        catch {
            NSLog("Unable to create event queue directory.")
        }
        
        let key = generateKey()
        let url = fullUrl(key: key)
        
        do {
            try data.write(to: url)
            return key
        }
        catch {
            NSLog("Unable to save event to event queue.")
        }
        return nil
    }
    
    class func getEvent(key: String) -> Data? {
        let url = fullUrl(key: key)
        return try? Data(contentsOf: url)
    }
    
    class func deleteEvent(key: String?) {
        if let key = key {
            let url = fullUrl(key: key)
            do {
                try FileManager.default.removeItem(at: url)
            }
            catch {
                NSLog("Unable to delete event from event queue.")
            }
        }
    }
    
    class func savedDataKeys() -> [String]? {
        let baseURL = baseUrl()
        do {
            // Get the URLs of all saved files in our directory
            let directoryContents = try FileManager.default.contentsOfDirectory(at: baseURL, includingPropertiesForKeys: nil, options: [])
            
            // URLs are a base url plus the key
            // Remove the base url so we're left with the keys
            let directoryContentsKeys = directoryContents.map({$0.absoluteString.replacingOccurrences(of: baseURL.absoluteString, with: "")  })
            return directoryContentsKeys
        }
        catch {
            NSLog("Unable to load saved events.")
        }
        return nil
    }
    
    class func baseUrl() -> URL {
        let filesKey = "caliperData"
        return try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true).appendingPathComponent(filesKey, isDirectory: true)
    }
    
    class func fullUrl(key: String) -> URL {
        let baseURL = baseUrl()
        return baseURL.appendingPathComponent(key)
    }
    
    class func generateKey() -> String {
        return UUID().uuidString
    }
    
}
