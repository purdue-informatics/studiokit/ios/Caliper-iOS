//
// Copyright 2017 Purdue University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

class Requestor {

    var urlSession: URLSession?
    var urlRequest: URLRequest?
    var sensor: Sensor?
    var completionHandler: ( (_ error: Error?, _ response: HTTPURLResponse?, _ data: Data?, _ caliperData: Data?) -> Void )?
    var keysBeingUploaded = [String]()
    var options: Options
    
    init(options: Options) {
        self.options = options
    }
    
    // MARK: Send
    func send(sensor: Sensor, data: BaseObject) {
        send(sensor: sensor, data: [data])
    }
    
    func send(sensor: Sensor, data: [BaseObject]) {
        self.sensor = sensor
        
        let envelope = createEnvelope(sensor: sensor, sendTime: Date(), data: data)
        
        let envelopeDictionary = envelope.envelopeDictionary()
        
        do {
            let data = try JSONSerialization.data(withJSONObject: envelopeDictionary, options: .prettyPrinted)
            
            uploadData(data: data)
        } catch {
            self.completionHandler?(CaliperError.invalidData, nil, nil, nil)
        }
    }
    
    func createEnvelope(sensor: Sensor, sendTime: Date, data: [BaseObject]) -> Envelope {
        return Envelope(sensor: sensor, sendTime: sendTime, data: data)
    }
    
    // MARK: Upload
    func uploadData(data: Data, key: String? = nil) {
        var eventKey: String?
        if let key = key {
            eventKey = key
        }
        else {
            eventKey = EventCache.saveEvent(data: data)
        }
        
        if options.logJSON {
            let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            NSLog(string as String? ?? "")
        }
        
        setConfigurationOptions()
        
        if var urlRequest = urlRequest {
            urlRequest.httpMethod = "POST"
            uploadData(urlRequest: urlRequest, caliperData: data, eventKey: eventKey)
        }
    }
    
    func uploadData(urlRequest: URLRequest, caliperData: Data, eventKey: String?) {
        
        // Prevent data that is currently being uploaded from being uploaded again
        // Could happen due to uploadSavedEvents()
        if isUploadingKey(key: eventKey) {
            return
        }
        
        let uploadTask = urlSession?.uploadTask(with: urlRequest, from: caliperData, completionHandler: {(data: Data?, response: URLResponse?, error: Error?) in
            
            self.removeUploadingKey(key: eventKey)
            
            if var error = error as NSError? {
                
                if error.domain == NSURLErrorDomain && (error.code == NSURLErrorNotConnectedToInternet || error.code == NSURLErrorNetworkConnectionLost) {
                    error = CaliperError.offline as NSError
                }
                self.completionHandler?(error, nil, nil, nil)
                return
            }
            
            guard let response = response else {
                // Unlikely to occur but handle anyways
                self.completionHandler?(CaliperError.other(message: "Received neither a response nor an error."), nil, nil, nil)
                return
            }
            
            // "Whenever you make an HTTP request, the NSURLResponse object you get back is actually an instance of the HTTPURLResponse class."
            // Therefore, explicitly convert response so the status code can be accessed
            guard let httpResponse = response as? HTTPURLResponse else {
                // Casting to HTTPURLResponse won't fail
                // But this is needed anyways for safety
                self.completionHandler?(CaliperError.other(message: "Received a bad response."), nil, nil, nil)
                return
            }
            
            if httpResponse.statusCode > 299 {
                // Failed
                
                // Delete events on bad requests
                // Otherwise, don't mess with it
                if httpResponse.statusCode == 400 || httpResponse.statusCode == 413 {
                    EventCache.deleteEvent(key: eventKey)
                }
                if httpResponse.statusCode == 401 {
                    self.completionHandler?(CaliperError.unauthorized, httpResponse, data, nil)
                    return
                }
                
                self.completionHandler?(CaliperError.unableToSendToStore(host: self.options.host), httpResponse, data, caliperData)
                return
            }
            else {
                // Success
                
                EventCache.deleteEvent(key: eventKey)
                
                // Now check if any more need to be uploaded
                self.uploadSavedEvents()
                
                self.completionHandler?(nil, httpResponse, data, nil)
                return
            }
            
        });
        
        // Start the uploadTask
        uploadTask?.resume()
    }
    
    // Upload one event at a time
    // Once one finishes, the next one uploads
    func uploadSavedEvents() {
        if let savedKeys = EventCache.savedDataKeys() {
            if savedKeys.count >= 1 {
                let key = savedKeys[0]
                if let data = EventCache.getEvent(key: key) {
                    uploadData(data: data, key: key)
                }
            }
        }
    }
    
    // MARK: Events being uploaded
    func isUploadingKey(key: String?) -> Bool {
        if let key = key {
            if keysBeingUploaded.contains(key) {
                return true
            }
            keysBeingUploaded.append(key)
        }
        return false
    }
    
    func removeUploadingKey(key: String?) {
        // There's not a remove method for arrays, so make the array equal to the array that has all items except the event key
        keysBeingUploaded = keysBeingUploaded.filter{$0 != key}
    }
    
    // MARK: Config
    func setConfigurationOptions() {
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = [
            "Authorization": options.apiKey ?? "",
            "Content-Type": "application/json",
        ]
        
        urlSession = URLSession(configuration: sessionConfiguration)
        
        let hostURL = URL(string: options.host)
        
        if let hostURL = hostURL {
            urlRequest = URLRequest(url: hostURL)
        }
        else {
            completionHandler?(CaliperError.invalidHost(host: options.host), nil, nil, nil)
        }
    }
    
}
