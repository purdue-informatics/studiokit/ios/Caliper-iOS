Pod::Spec.new do |s|
  s.name         = 'Caliper-iOS'
  s.version      = '1.0.4'
  s.summary      = 'Caliper 1.0 Sensor Library for iOS.'

  s.homepage         = "https://github.com/purdue-tlt/Caliper-iOS"
  s.license          = { :type => "Apache 2.0"}
  s.author           = "Purdue University"
  s.documentation_url = "https://purdue-tlt.github.io/Caliper-iOS/"
  s.source           = { :git => "https://github.com/purdue-tlt/Caliper-iOS.git", :tag => "1.0.4" }
  s.source_files     = 'Caliper/**/*.swift'

  s.ios.deployment_target     = '8.0'
end
